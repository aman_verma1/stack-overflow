from datetime import datetime
from app import db, login
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

question_tags = db.Table('question_tags',
    db.Column('question_id', db.Integer, db.ForeignKey('question.question_id')),
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.tag_id'))
)


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))


    def __repr__(self):
        return '<User {}>'.format(self.username)
    
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    


@login.user_loader
def load_user(id):
    return User.query.get(int(id))



class question(db.Model):
    question_id = db.Column(db.Integer, primary_key=True)
    title=db.Column(db.Text())
    content = db.Column(db.Text())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    tags=db.relationship('tag',secondary=question_tags,backref=db.backref('tags',lazy='dynamic'))

    

    def __repr__(self):
        return '<question {}>'.format(self.title)
    
    def tag_question(self):
        return tag.query.join(
            question_tags, (question_tags.c.tag_id == tag.tag_id)).filter(
                question_tags.c.question_id == self.question_id) 

class answer(db.Model):
    answer_id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text())
    q_id=db.Column(db.Integer, db.ForeignKey('question.question_id'))

class tag(db.Model):
    tag_id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text())




