from flask import render_template, flash, redirect, url_for, request, abort
from flask_login import login_user, logout_user, current_user, login_required
from werkzeug.urls import url_parse
from app import app, db
from app.forms import LoginForm, RegistrationForm, QuestionForm, AnswerForm, EditQuestionForm
from app.models import User, question, answer, tag, question_tags
from sqlalchemy.sql import exists


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    iterate_question = question.query.all()
    return render_template('index.html',
                           title='question',
                           question1=iterate_question)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/ask-question', methods=['GET', 'POST'])
def ask_question():
    form = QuestionForm()
    if form.validate_on_submit():
        question1 = question(title=form.title.data,
                             content=form.body.data,
                             user_id=current_user.id)
        db.session.add(question1)
        db.session.commit()
        tag_content = form.tag.data
        tag_content1 = tag_content.split(',')
        for u in tag_content1:
            if (db.session.query(
                    tag.query.filter(tag.content == u).exists()).scalar()):
                each_tag = tag.query.filter_by(content=u).first()
                question1.tags.append(each_tag)
                db.session.commit()
                continue
            tag_data_content = tag(content=u)
            db.session.add(tag_data_content)
            question1.tags.append(tag_data_content)
            db.session.commit()

        flash('Your question is recorded!')
        return redirect(url_for('index'))
    return render_template('ask-question.html', form=form)


@app.route('/question-description/<question_id>', methods=['GET', 'POST'])
def question_description(question_id):
    each_question = question.query.filter_by(question_id=question_id).first()
    if each_question is None:
        abort(404)
    tags = each_question.tag_question()
    form = AnswerForm()
    if form.validate_on_submit():
        answer1 = answer(content=form.body.data, q_id=question_id)
        db.session.add(answer1)

        db.session.commit()

    iterate_answer = answer.query.filter_by(q_id=question_id)

    return render_template('question-description.html',
                           answer=iterate_answer,
                           question=each_question,
                           form=form,
                           tags=tags,
                           current_user_id=current_user.id,
                           each_question_user_id=each_question.user_id)


@app.route('/question-description/<question_id>/edit', methods=['GET', 'POST'])
def edit(question_id):
    form = EditQuestionForm()
    each_question = question.query.filter_by(question_id=question_id).first()
    if each_question is None:
        abort(404)

    if (int(each_question.user_id) != int(current_user.id)):
        abort(403)
    if form.validate_on_submit():
        each_question.title = form.title.data
        each_question.content = form.body.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(
            url_for('question_description', question_id=question_id))
    form.title.data = each_question.title
    form.body.data = each_question.content

    return render_template('edit.html', form=form, each_question=each_question)
